.. winvnkey-site documentation master file, created by
   sphinx-quickstart on Sat Mar 14 13:13:09 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to WinVNKey!
=========================================

**WinVNKey is a free Windows-based keyboard driver for typing Vietnamese and many other languages in the world.**

* WinVNKey supports most, if not all, Vietnamese   character sets (encodings) known so far. For example, it supports Vietnamese Unicode, VISCII, VNI, VPS, ABC, ... as well as obsolete Vietnamese character sets used in the 1980’s. It can also be used to type many national languages such as French, German, Polish, Czech, Russian, Pali, Pinyin, Japanese, etc.. In particular, users can obtain Hán (Chinese) and Nôm (Vietnamese classic) characters by either typing their pronunciations in Vietnamese or Pinyin, or entering numerical codes.
 
* WinVNKey has a very smart algorithm to handle diacritical mark placement and to correct spelling.  Users can type the marks anywhere in the syllable of a Vietnamese word and WinVNKey can figure out if the word is Vietnamese and will position them accordingly. If adding a new character to an existing Vietnamese word makes it non Vietnamese, WinVNKey can automatically undo all the marks that were typed for that word if desired.  This feature is very useful when typing text in mixed languages such as Vietnamese and English.  In addition, WinVNKey can correct spelling on the fly (while typing) for single, double, and triple words based on a combination of grammar rules and databases. 
 
  ``Users can also turn on Hán Nôm mode where typing Vietnamese or Pinyin pronunciations, or some other numerical codes, will result in Hán and Nôm characters. WinVNKey maintains a very large database of Hán Nôm characters based on the Unicode standard. It provides an interface for users to update the Hán Nôm database with files from the latest Unicode standard without waiting for the next release of WinVNKey. Users can also define their own characters and how to type them.``
 
* Support for conversion of character sets in plain text or rich text (RTF) is very powerful. About a hundred character sets are supported, which allow users to convert from any character set to any other character set. Users can also add new character sets simply by providing a file that contains the mapping of that character set to Unicode.
 
* WinVNKey is unique in the market regarding support for converting Vietnamese documents in MS Word that contain fonts from different character sets. For example, an old Word document can contain ABC fonts, VNI fonts, Microsoft fonts, etc. This means it contain characters from at least three character sets: TCVN, VNI, and Unicode. To convert this document to Unicode, users simply save the file as an RTF document, then run the converter in WinVNKey. The converter will list the names of all the fonts used in the document and ask the users to specify the new fonts, one for each old font.
 
  ``The key strength of WinVNKey is its powerful support for macro processing and setting customization that can meet the need of the most demanding users.   WinVNKey exposes hundreds of settings to users.  The product is shipped with a number of predefined sets of settings stored in text files.  Users can modify these files directly, or via the user interface, to define their own typing methods and macros. There are no limits on the number of macros users can define and load. Many users have reported using hundreds of thousands of Vietnamese macros in WinVNKey without degrading its performance.  This is a good testament to the extremely fast and efficient algorithm in WinVNKey that handles both macros and Hán Nôm typing.  Note that if Hán Nôm typing is enabled, WinVNKey will have to handle hundreds of thousands of Hán Nôm entries.  Thus, even if a user thinks he does not use any Vietnamese macros, WinVNKey actually works hard behind his back if he turns on Hán Nôm but he would not see any slowdown in performance.``

Screenshots
===========

.. image:: _static/screenshots/mainwinvnkey.jpg
.. image:: _static/screenshots/languages.jpg
.. image:: _static/screenshots/taskbar.jpg

Index
=====

.. toctree::
   :maxdepth: 2

   download
   documentation   
   news
   faq
   forum
   about

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`