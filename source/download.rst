How to get WinVNKey
===================
| Want to type Vietnamese, Hán Nôm, other foreign languages or adapt the Quick Vietnamese Entry method.
| WinVNKey is coming here!

Beta Version
------------
* | Latest: `WinVNKey 5.5.463 <http://winvnkey.sourceforge.net/beta/download/>`_ **RECOMMENDED**  
  | which fully support the new Quick Vietnamese Entry method (the so-called fastest Vietnamese typing method: Tubinhtran-MS).

`Browse files <http://winvnkey.sourceforge.net/beta/download/>`_

Official Version
----------------
* Latest: `WinVNKey 5.5.456 <https://sourceforge.net/projects/winvnkey/files/WinVNKey%20Official%20Rel/>`_

  * For `Windows Vista 32bit and 64bit <https://sourceforge.net/projects/winvnkey/files/WinVNKey%20Official%20Releases/winvnkey%205.5.456%20for%20Vista%2032-bit%20and%2064-bit/>`_
  * For `Windows NT/2000/XP/2003/Vista/2008 <https://sourceforge.net/projects/winvnkey/files/WinVNKey%20Official%20Releases/winvnkey%205.5.456%20for%20NT_2000_XP_2003_Vista_2008/>`_

`Browse files <https://sourceforge.net/projects/winvnkey/files/WinVNKey%20Official%20Releases/>`_

Fonts
-----
* `Hán Nôm <http://winvnkey.sourceforge.net/qua/>`_

  * Han Nom A
  * Han Nom B
  * Arial Unicode

* `Viet Unicode <https://sourceforge.net/projects/winvnkey/files/Viet%20Unicode%20Fonts/>`_
* `VISCII <https://sourceforge.net/projects/winvnkey/files/VISCII%20VN%20Fonts/>`_


`Browse all <https://sourceforge.net/projects/winvnkey/files/>`_

How to install WinVNKey
=======================
Read the documentation

